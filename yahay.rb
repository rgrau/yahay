# -*- coding: utf-8 -*-
require 'rubygems'
require 'sinatra'
require '3scale_client'
require 'json'

helpers do
  def sanitize_word(word)
    word.gsub(/[ !.,]/, '').
      tr("áÁéÉíÍóÓúÚü", "aaeeiioouuu")
  end

  def sorted_and_normalized(word)
    sanitize_word(word).chars.sort
  end
end

get("/") do
  erb :main
end

get '/v1/anagram' do
  return {
    word1: params[:word1],
    word2: params[:word2],
    is_anagram: sorted_and_normalized(params[:word1]) == sorted_and_normalized(params[:word2]) }.to_json
end

get("/v1/palindrome") do
  puts sanitize_word(params[:word1]).reverse
  puts sanitize_word(params[:word1])
  return {
    word1: params[:word1],
    is_palindrome: sanitize_word(params[:word1]) == sanitize_word(params[:word1]).reverse }.to_json
end
